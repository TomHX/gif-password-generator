<?php 


/* 

Plugin Name: Wrong Password Giphy Generator
Description: Spice up those incorrect login attempts by introducing a gif of your choice to display after a failed attempt.
Version: 1.0
Author: Tom Haxell
Author URI: tomhaxell.com
License: GPL2

*/ 

// Add our js script

function gif_pass_scripts() {
	wp_register_script('pass_script', plugins_url('scripts.js', __FILE__),
	array('jquery'), '1.1', true);
	wp_enqueue_script('pass_script');
	wp_localize_script('pass_script', 'pass_search_value', 
					array(
						'the_plug_key' => get_option('pass_search_text'),
						)
					);
}


add_action('wp_enqueue_scripts', 'gif_pass_scripts');

add_action('admin_enqueue_scripts', 'gif_pass_scripts');

// Add jquery to the login page 

function gif_pass_jquery_for_login() {
	wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js' );
	wp_enqueue_script('jquery' );
}

add_action('login_enqueue_scripts', 'gif_pass_jquery_for_login');

// Add our js to the login page 

add_action('login_enqueue_scripts', 'gif_pass_scripts');

// Add our stylesheet

function gif_pass_style() {
	wp_register_style('pass_style', plugins_url('pass_style.css', __FILE__), false);
	wp_enqueue_style('pass_style');
}

add_action('admin_enqueue_scripts', 'gif_pass_style');

// Add our stylesheet to the login page 

add_action('login_enqueue_scripts', 'gif_pass_style');


// Register our settings

add_action('admin_init', 'pass_plugin_settings_register');

function pass_plugin_settings_register() {
	register_setting('pass-plugin-settings-group', 'pass_display_text');
	register_setting('pass-plugin-settings-group', 'pass_search_text');
}


// Create Option Page 

function pass_plugin_menu() {
	add_menu_page('Wrong Password Settings', 'Wrong Password Settings', 'administrator', 'pass-plugin-settings', 'pass_plugin_settings_page', 'dashicons-admin-generic');
}

add_action('admin_menu', 'pass_plugin_menu');


// Localize our JS


function pass_plugin_settings_page() {
	?>

	<div class="wrap">
		<h1 class="pass-h1">Wrong Password Giphy Generator</h1>
		<form method='POST' action="options.php">
		<?php settings_fields( 'pass-plugin-settings-group'); ?>
		<?php do_settings_sections('pass-plugin-settings-group'); ?>
			<div class="plug__option_one">
				<h2 class="pass-h2">Text you want displayed when a failed attempt occurs</h2>
				<input id="pass_display_text_option_one" class="plug_pass_input" type='text' name='pass_display_text' value="<?php echo esc_attr( get_option('pass_display_text')); ?>">
			</div>
			<h2 class="pass-h2"> Or </h2>
			<div class="plug__option_two">
				<h2 class="pass-h2"> Search for a Gif to display </h2>
				<input id="pass_display_search_option_two" class="plug_pass_input" type="text" name="pass_search_text" value="<?php echo esc_attr( get_option('pass_search_text')); ?>">
			</div>
			<div class="plug_result" data="<?php echo esc_attr( get_option('pass_search_text')); ?>">
				<?php
				$pass_search_variable = get_option('pass_search_text'); ?>	
				<?php echo $pass_search_variable; ?>	
			</div>			
			<?php submit_button(); ?>
		</form>
	</div>

	<?php
}


// Hook into the login error section

add_filter('login_errors', 'login_error_message' );

function login_error_message( $error ) {
	
	if(get_option('pass_display_text')) {
		$error = get_option('pass_display_text');
		return $error;
	}
	else {
		$gif_error ="<img id='pass_plug_gif_error' src=''>";
		return $gif_error;
	}
}




















